use std::rc::Rc;

use grx::{
    button, component, grx, header_bar, icon,
    menu::{self, item::item, Menu},
    props, Component,
};

use crate::store::Action;

#[props]
#[derive(Debug, Default)]
pub struct Props {}

#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct State {}

#[component(State)]
pub struct Root {}

pub fn header(_props: Props) -> Component {
    let menu: Rc<Menu>;

    grx! {
        header_bar [
            menu (id=menu, items=vec![
                item("Open", Some(Action::Start)),
                item("Quit", Some(Action::Quit)),
            ]),
            button (on_click=move || menu.show()) [
                icon("open-menu-symbolic")
            ]
        ]
    }
}
