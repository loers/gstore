// SPDX-License-Identifier: GPL-3.0-or-later

//! # gstore
//!
//! Global and local state management for GTK apps written in Rust.
//!
extern crate log;

mod store;

pub use store::*;

// Reexports
pub use once_cell;

pub const INIT: &str = "init";

// #[cfg(feature = "gtk")]
mod gtk;
