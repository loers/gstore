# gstore

[![pipeline status](https://gitlab.com/floers/gstore/badges/main/pipeline.svg)](https://gitlab.com/floers/gstore/-/commits/main)
[![API](https://docs.rs/gstore/badge.svg)](https://docs.rs/gstore)

Global management for for grx rust apps.

```

## License

gstore is distributed under the terms of the GPL-3.0 license. See LICENSE for details.
